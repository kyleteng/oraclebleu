#include "CandGroup.h"
#include "StringHelper.h"

CandGroups::CandGroups(){}

CandGroups::CandGroups(istream& sin)
{
	getline(sin,this->source);
	string line; 
	int num_of_cand;
	getline(sin,line);
	transfer(line,num_of_cand);

	this->candidates.resize(num_of_cand);
	
	int cand_index = 0;
	while(cand_index < num_of_cand)
	{
		//cout<<"��"<<cand_index<<"��"<<endl;
		getline(sin,line);
		Cand * cand = new Cand(line);
		this->candidates[ cand_index++ ] = cand;
	}
}

CandGroups::~CandGroups()
{
	while(!this->candidates.empty())
	{
		delete this->candidates.back();
		this->candidates.pop_back();
	}
}

ostream& operator<<(ostream& out, const CandGroups& group)
{
	out<< group.source<<endl;
	out<<group.candidates.size() <<endl;
	vector<Cand*>::const_iterator  cand_iter = group.candidates.begin();
	for( ; cand_iter != group.candidates.end(); cand_iter++)
		out<<*(*cand_iter);
	return out;
}



