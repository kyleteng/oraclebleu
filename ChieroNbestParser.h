#ifndef _CHIERO_NBEST_PARSER
#define _CHIERO_NBEST_PARSER
#include <string> 
#include <vector>
#include "CandGroup.h"

using namespace std;

class ChieroNbestParser{
public:
	//解析
	ChieroNbestParser(const string& filename);
	~ChieroNbestParser();
	friend ostream& operator<<(ostream& out, const ChieroNbestParser& parser);
	inline vector<CandGroups*>& Groups()
	{
		return this->nbest_groups;
	}
	inline const vector<CandGroups*>& Groups() const
	{
		return this->nbest_groups;
	}
private:
	//第一维是nbest组的编号，第二维是句子编号，第三维是token
    vector<CandGroups* >nbest_groups;
	void parse(const string& filename);
};
#endif