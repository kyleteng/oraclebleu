#ifndef _ORACLE_SCORER_H
#define _ORACLE_SCORER_H
#include "BLEUScorer.h"
#include "ReferenceParser.h"
#include "ChieroNbestParser.h"
#include "NGram.h"
#include <map>
//ORACLE元组 串和相应的分数
#define ORACLE pair<Cand*,float> 

class OracleScorer
{
public:
	//构建一个计算oracle的程序
	OracleScorer(const string& nbest_file, const string& ref_file);
	//计算oracle
	void calculateOracle();
	//保存
	void save(const string& file) const;
private:
	//计算某一个句子的oracle
	void calculateOracle(const CandGroups* group, const vector<NGram>& refs , ORACLE& oracle);
	//保存成文本文件
	void save_plain_text(const string& file) const;
	//保存成sgm
	void save_sgm_text(const string& file) const ;
	//保存成compare的文件
	void save_compare_text(const string& file) const;

	ChieroNbestParser nbest_parser;
	ReferenceParser ref_parser;
	BLEUScorer bleu_scorer;
	//保留每个nbest中最佳的结果
	vector<ORACLE> oracles;
};
#endif