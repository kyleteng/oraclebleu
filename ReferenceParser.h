#ifndef _REFERENCE_PARSER_H
#define _REFERENCE_PARSER_H
#include <string>
#include <vector>
#include <iostream>

#define SEG string
#define DOC vector<SEG>

using namespace std;

class ReferenceParser{
public:
	ReferenceParser(const std::string& file);
	inline vector<DOC >& Refs()
	{
		return this->references;
	}
	inline const vector<DOC >& Refs() const
	{
		return this->references;
	}
	friend ostream& operator<<(ostream& out, const ReferenceParser& refs);
private:
	void parse(const std::string& file);
	void parse_doc(istream& fin);
	void parse_seg(const string& line, string& seg);
	//多个DOC的集合
	vector<DOC > references;
};
#endif