#include <iostream>
#include "OracleScorer.h"
#include "BLEUScorerTester.h"

int main(int argc , char* argv[])
{
    if(argc!=4){
        cerr<<"Usage: ./oracle_bleuer ref_file kbest_file output_file"<<endl;
        return 0; 
    }
	const string& ref_file = argv[1];
	const string& kbest_file = argv[2];
	const string& output_file = argv[3];

	OracleScorer scorer(kbest_file,ref_file);
	scorer.calculateOracle();
	scorer.save(output_file);
    
	/*BLEUScorerTester tester;
	tester.test();*/
    
	return 0;
}
