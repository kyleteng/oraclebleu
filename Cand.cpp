#include "Cand.h"
#include "StringHelper.h"
#include <iostream>
#include <iterator>

Cand::Cand():score(0){}

Cand::Cand(const string& line)
{
	//the flight number is c three zero six . ||| -201.81 -226.796 -209.494 -209.605 1 5 11 -15.7578 -19.9238  ||| -51.1683
	vector<string> line_groups;
	split(line,"|||", line_groups);
	if(line_groups.size() != 3 ) 
	{
		cerr << "format error: " <<line<<endl;
		exit(-1);
	}
	//处理 the flight number is c three zero six . 
	token(line_groups[0],this->grams);
	//-201.81 -226.796 -209.494 -209.605 1 5 11 -15.7578 -19.9238
	token(line_groups[1],this->features);
	//处理最后的score
	transfer(line_groups[2],this->score);
}

ostream& operator<<(ostream& out, const Cand& cand)
{
	copy(cand.grams.begin(),cand.grams.end(),ostream_iterator<string>(out," "));
	out<<" ||| ";
	copy(cand.features.begin(),cand.features.end(),ostream_iterator<float>(out," "));
	out<<" ||| ";
	out<< cand.score<<endl;
	return out;
}