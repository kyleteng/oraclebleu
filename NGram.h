#ifndef _NGRAM_H
#define _NGRAM_H
#include <string>
#include <vector>
#include <map>
using namespace std;
//表示一个句子的ngram数
class NGram{
public:
	NGram();
	NGram(const string& sentences,int n = 4);
	//n表示元数
	NGram(const vector<string>& grams,int n = 4);
	NGram& operator=(const string& sentences);
	NGram& operator=(const vector<string>& grams);
	
	inline const vector<int>& Length() const
	{
		return this->length;
	}
	inline  vector<int>&  Length()
	{
		return this->length;
	}
	inline  vector<map<string,int> >&  Grams()
	{
		return this->ngrams;
	}
	inline const vector<map<string,int> >&  Grams() const
	{
		return this->ngrams;
	}
private:
	/**
	  抽取一个句子的n-gram
	  @param: grams 句子中的词
	  @param: n n-gram的n
	*/
	void enumerateNgram(const vector<string>& grams,int n) ; 

	//从字符串开始构造
	void construct(const string& sentence, int n = 4);
	//从数组开始构造
	void construct(const vector<string>& sentence, int n = 4);

	//下标表示元数-1 ，n-gram  ngrams[n-1]
	vector<map<string,int> > ngrams;
	//表示长度，就不用再算了,每一个gram的长度
	vector<int> length;
};
#endif