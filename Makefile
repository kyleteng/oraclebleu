#program name
PROGRAM=oracle_bleuer
#complier to user
CC=g++
#options I'll pass to the complier
CFLAGS=-Wall -O3 -c -funroll-loops

LDFLAGS=-lz
#directory of obj files
OBJDIR=bin
#header files
HEADERS=-Iinclude/
#source files
SOURCES=$(wildcard *.cpp)
#target objects
OBJECTS=$(SOURCES:.cpp=.o)


#default target
all: | ObjDir
all: $(PROGRAM)
#Main target
$(PROGRAM): $(OBJECTS)
	$(CC)  $(HEADERS) -o $(OBJDIR)/$@ $(addprefix $(OBJDIR)/,$(OBJECTS)) $(LDFLAGS)
#pattern rule
%.o : %.cpp
	$(CC) $(CFLAGS) $(CPPFLAGS) $(HEADERS) $< -o  $(OBJDIR)/$@
#execute dir
ObjDir:
	mkdir -p $(OBJDIR)

.PHONY : clean ObjDir cleanall

clean :
	-rm $(OBJDIR)/$(PROGRAM) $(OBJDIR)/*.o 
cleanall:
	-rm *.o $(OBJDIR)/$(PROGRAM)

