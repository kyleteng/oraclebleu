#include "OracleScorer.h"
#include "StringHelper.h"
#include <fstream>

OracleScorer::OracleScorer(const string& nbest_file, const string& ref_file):\
	nbest_parser(nbest_file),ref_parser(ref_file){}

void OracleScorer::calculateOracle()
{
	//遍历nbest的每一个Group
	int sent_index = 0 ;
	int sent_size = nbest_parser.Groups().size();
	this->oracles.resize(sent_size);
	for( ; sent_index < sent_size; sent_index++)
	{
		//1,某一句话的nbest CandGroups*
		//nbest_parser.Groups()[sent_index]; 
		//2,该句话的ref_集合
		unsigned int doc_index = 0;
		vector<NGram> refs(ref_parser.Refs().size() );
		for( ; doc_index < ref_parser.Refs().size() ;doc_index++)
					refs[doc_index] = NGram(ref_parser.Refs()[doc_index][sent_index]);
		//3,计算
		this->calculateOracle(nbest_parser.Groups()[sent_index], refs,this->oracles[sent_index] );
	}
}

void OracleScorer::calculateOracle(const CandGroups* groups, const vector<NGram>& refs, ORACLE& oracle)
{
	float max_bleu = 0.0;
	vector<Cand*>::const_iterator cand_iter = groups->Cands().begin();
	//弄成grams
	vector<Cand*>::const_iterator max_iter = cand_iter;
	//int n = 1;
	for(; cand_iter != groups->Cands().end() ; cand_iter++)
	{
		//cout<<endl<<groups->Source()<<endl;
		//cout<< n++<<endl;
		NGram cand_gram((*cand_iter)->Grams());
		float bleu = this->bleu_scorer.calculate(cand_gram, refs);
		if( max_bleu < bleu)
		{
			max_bleu = bleu;
			max_iter = cand_iter;
		}
	}
	oracle.first = *max_iter;
	oracle.second = max_bleu;
}

void OracleScorer::save_compare_text(const string& file) const
{
	//先输出原文
	//然后输出候选的译文，然后输出最好的分数
	ofstream fout(file.c_str());
	unsigned int sent_index = 0;
	for(; sent_index < this->oracles.size() ;sent_index++)
	{
		//需要输出这个句子的特征，跟最好的比较
		fout<<(*(this->nbest_parser.Groups()[sent_index]->Cands()[0]));
		fout<<(*(this->oracles[sent_index].first));
		fout<<this->oracles[sent_index].second<<endl;
		fout<<endl;
	}
	fout.close();
}

void OracleScorer::save_plain_text(const string& file) const
{
	//先输出原文
	//然后输出候选的译文，然后输出最好的分数
	ofstream fout(file.c_str());
	unsigned int sent_index = 0;
	for(; sent_index < this->oracles.size() ;sent_index++)
		fout<<this->oracles[sent_index].first;
	fout.close();
}

void OracleScorer::save_sgm_text(const string& file) const 
{

}

void OracleScorer::save(const string& file) const 
{
	this->save_plain_text(file+".plain");
	this->save_compare_text(file+".cmp");
	this->save_sgm_text(file+".sgm");
}
