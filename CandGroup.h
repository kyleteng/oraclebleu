#ifndef _CAND_GROUP_H
#define _CAND_GROUP_H
#include "Cand.h"
class CandGroups
{
public:
	CandGroups();
	CandGroups(istream& sin);
	~CandGroups();
	friend ostream& operator<<(ostream& out, const CandGroups& group);
	inline vector<Cand* >& Cands()
	{
		return this->candidates;
	}
	inline const vector<Cand* >& Cands() const
	{
		return this->candidates;
	}
	inline string& Source()
	{
		return this->source;
	}
	inline const string& Source() const
	{
		return this->source;
	}
private:
	//原文 sent: <s> 哦 那个 航班 是 c 三零六 。 </s> 直接保留
	string source;
	vector<Cand* > candidates;
};
#endif