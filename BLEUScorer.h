#ifndef _BLEU_SCORER_H
#define _BLEU_SCORER_H
#include <string>
#include <vector>
#include "NGram.h"
using namespace std;
class BLEUScorer{
public:
	/**
	   计算与多个参考集的BLEU
	   @param: output_grams 输出的翻译结果的ngram
	   @param: ref_group_grams 参考集的ngram
	 */
	float calculate(const NGram& output_grams, const vector<NGram>& ref_group_grams) const;
private:
	/**
	  计算BLEU中的Brevity的部分
	  不考虑平滑带来的影响
	  @param: output_grams 输出的翻译结果
	  @param: ref_grams 参考的结果
	*/
	float calculateBrevity(const int& c,const int& r) const ;
	/**
	  计算ngram匹配的精度
	  @param: cand_gram 翻译的结果
	  @param: ref_grams 参考集的结果
	*/
	float calculatePrecision(const NGram& cand_gram , const vector<NGram>& ref_grams) const;
	/**
	  计算某一元的n-gram匹配的的分数
	  @param: cand_gram 输出的翻译结果
	  @param: ref_grams 参考的结果
	  @param: n gram的数，最多为4
	*/
	void calculateNgramPrecision(const NGram& cand_gram, const vector<NGram>& ref_grams,float& score, float& smooth_score, int n ) const;
	/**
	计算两个n_grams的集合中相交的gram的个数
	@param: output_n_grams 翻译输出的ngram集合
	@param: ref_n_grams 参考的ngram集合
	*/
	int calculateNgramCount(const NGram& output_n_grams, const vector<NGram>& ref_n_grams,int n) const;
	/**
	  获得与候选译文最近的长度
	  @param: cand_gram 候选的翻译
	  @param: group_grams 参考集
	 */
	int calculateClosestLength(const NGram& cand_gram, const vector<NGram>& ref_grams) const ;
};
#endif
