#include "BLEUScorer.h"
#include <math.h>
#include <iostream>
#include <iterator>
#include <limits.h>
#include <stdlib.h>

float BLEUScorer::calculateBrevity(const int& c,const int& r) const
{
	float BP = 0.0;
	if(c> r)
		BP = 1;
	else
		BP = exp(1-r*1.0f/c);
	return BP;
}

//BLEU+1
void BLEUScorer::calculateNgramPrecision(const NGram& cand_gram, const vector<NGram>& ref_grams,float& score, float& smooth_score, int n) const 
{
	//求交集
	 int matches = this->calculateNgramCount(cand_gram,ref_grams,n);
	 //cout<<"matches:"<<matches<<endl;
	//+1平滑
	 score =  (matches*1.0f+1)/ (cand_gram.Length()[n-1]+1);
	 //计算要减去的平滑的分数
	 smooth_score = 1.0f /(cand_gram.Length()[n-1]+1);
}

int BLEUScorer::calculateNgramCount(const NGram& cand_gram , const vector<NGram>& ref_grams,int n) const
{
	int sum_match = 0;
	// count = min( count, Max_Ref_count);
	int gram_index = n-1;
	map<string,int>::const_iterator cand_gram_iter = cand_gram.Grams()[gram_index].begin();
	for( ; cand_gram_iter != cand_gram.Grams()[gram_index].end() ; cand_gram_iter++)
	{
		int count = cand_gram_iter->second;
		//cout<< cand_gram_iter->first<<endl;
		//max_ref_count
		int max_ref_count = 0;
		vector<NGram>::const_iterator ref_iter = ref_grams.begin() ;
		for(; ref_iter != ref_grams.end() ;ref_iter++)
		{
			map<string,int>::const_iterator ref_gram_iter = ref_iter->Grams()[gram_index].find(cand_gram_iter->first);
			if(ref_gram_iter != ref_iter->Grams()[gram_index].end())
				if(max_ref_count < ref_gram_iter->second)
					max_ref_count = ref_gram_iter->second;
		}
		sum_match += min(count, max_ref_count);
	}
	//cout << "match: "<<sum_match<<endl;
	return sum_match;
}

float BLEUScorer::calculate(const NGram& output_grams, const vector<NGram >& ref_group_grams) const 
{
	//enumerate调用太多次了,可能接口还要改
	//1,计算长度
	int r = this->calculateClosestLength(output_grams, ref_group_grams);
	//cout<<"length:"<<r<<endl;
	//2,smooth BP
	float BP = this->calculateBrevity(output_grams.Length()[0],r+1);
	//cout<<"BP:"<<BP<<endl;
	//3, 计算ngram的分数
	float PC = this->calculatePrecision(output_grams,ref_group_grams);
	//cout<<"PC:"<<PC<<endl;

	return BP*PC;
}

int BLEUScorer::calculateClosestLength(const NGram& cand_gram, const vector<NGram >& ref_group_grams) const 
{
	//找一个最接近的
	int cand_len = cand_gram.Length()[0];
	int min_distance = INT_MAX;
	int min_index = 0;
	for(int gram_index = 0 ; gram_index < (int)ref_group_grams.size()  ;gram_index++)
	{
		int distance = abs(ref_group_grams[gram_index].Length()[0] - cand_len);
		if( min_distance > distance)
		{
			min_distance = distance;
			min_index = gram_index;
		}
	}
	return ref_group_grams[ min_index].Length()[0];
}
//PC Grounded
float BLEUScorer::calculatePrecision(const NGram& cand_gram, const vector<NGram> &ref_grams) const
{
	int max_n_gram = 4;
	vector<float> scores(max_n_gram);
	float total_score = 0;
	float smooth_score = 0;
	float ground_score = 0;
	for( int gram_index = 1 ; gram_index <= max_n_gram ; gram_index++)
	{
		//cout<<"calc bleu for gram:" << gram_index <<endl;
	    this->calculateNgramPrecision(cand_gram,ref_grams,scores[gram_index-1], ground_score,gram_index);
		//cout<<"\t"<<gram_index<<"gram:"<<scores[gram_index-1]<<endl;
		total_score += log(scores[gram_index-1]);
		smooth_score += log(ground_score);
		//cout<<endl;
	}
	total_score = exp(total_score/max_n_gram) - exp(smooth_score/max_n_gram);
	return total_score;
}
