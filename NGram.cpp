#include "NGram.h"
#include "StringHelper.h"
#include <iostream>

NGram::NGram(){};

void NGram::construct(const string& sentence, int n)
{
	vector<string> grams;
	//转小写
	string content = sentence;
	tolowercase(content);
	token(sentence,grams);
	this->construct(grams,n);
}

void NGram::construct(const vector<string>& grams, int n )
{
	this->length.resize(n);
	this->ngrams.resize(n);
	//枚举各个元数的并进行计数
	while(n)
	{
		this->enumerateNgram(grams,n);
		n--;
	}
}

NGram::NGram(const string& sentence, int n )
{
	this->construct(sentence,n);
}

NGram::NGram(const vector<string>& grams, int n)//:length(grams.size()),ngrams(n)
{
	this->construct(grams,n);
}

NGram& NGram::operator=(const string& sentence)
{
	this->construct(sentence);
	return *this;
}

NGram& NGram::operator=(const vector<string>& grams)
{
	this->construct(grams);
	return *this;
}

void NGram::enumerateNgram(const vector<string>& grams, int n)
{
	map<string,int>& gram_map = this->ngrams[n-1];
	for(unsigned int output_index = 0 ; output_index+n <= grams.size() ; output_index++)
	{
		 string n_gram ="";
	     for(unsigned int inner_index = output_index; inner_index < output_index+ n; inner_index++)
		 {
			 if( inner_index != output_index)
				  n_gram += "_";
			 n_gram +=  grams[inner_index];
		 }
		 gram_map[n_gram]++;
		 this->length[n-1]++; //记录长度
	}
	//for debug
	/*cout<<"grams for : "<<n<<endl;
	map<string,int>::iterator gram_iter = gram_map.begin();
	for(; gram_iter !=gram_map.end() ; gram_iter++)
		cout<<gram_iter->first <<"\t"<<gram_iter->second<<endl;
	*/
}