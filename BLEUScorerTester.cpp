#include "BLEUScorerTester.h"
#include <iostream>
void BLEUScorerTester::test() const
{
	string line = "sorry , it 's fully occupied .";
	NGram cand_gram(line);

	vector<NGram> refs;
	refs.push_back(line);

	float bleu = this->scorer.calculate(cand_gram,refs);
	cout <<"BLEU:"<<bleu<<endl;
}