#include "BLEUScorer.h"

class BLEUScorerTester
{
public:
	void test() const;
private:
	BLEUScorer scorer;
};