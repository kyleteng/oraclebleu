#ifndef _OUTPUT_CANDIDATE_H
#define _OUTPUT_CANDIDATE_H
#include <string>
#include <vector>
#include <iostream>
using namespace std;
//GRAM的结构

class Cand{
public:
	//构造函数
	Cand();
	Cand(const string& line);
	inline vector<float>& Features()
	{
		return this->features;
	}
	inline const vector<float>& Features() const
	{
		return this->features;
	}
	inline float& Score()
	{
		return this->score;
	}
	inline const float& Score() const
	{
		return this->score;
	}
	inline vector<string>& Grams()
	{
		return this->grams;
	}
	inline const vector<string>& Grams() const
	{
		return this->grams;
	}
	//用来取特征
	inline float operator()(int index)
	{
		return this->features[index];
	}
	inline const float& operator()(int index) const
	{
		return this->features[index];
	}
	friend ostream& operator<<(ostream& out, const Cand& cand);
private:
	float score ;
    vector<string>  grams;//这个里面没有位置关系
	vector<float>  features;
};
#endif