#include "ChieroNbestParser.h"
#include <fstream>
#include <iostream>
#include "StringHelper.h"
using namespace std;

ChieroNbestParser::ChieroNbestParser(const string& filename)
{
	this->parse(filename);
	cout<<"文件: "<<filename<<"解析完毕"<<endl;
}

void ChieroNbestParser::parse(const string& file)
{
	ifstream fin(file.c_str());
	if(!fin){
		cerr<<"file:"<<file<<" cannot open!"<<endl;
		exit(-1);
	}
	//组数
	int num_of_group;
	string line;
	getline(fin,line);
	transfer(line,num_of_group);

	this->nbest_groups.resize(num_of_group);
	int group_index = 0;
	while(group_index < num_of_group)
	{
		//cout<<"第"<<group_index<<"个"<<endl;
		CandGroups* groups = new CandGroups(fin);
		this->nbest_groups[group_index++] = groups;
	}
	fin.close();
}

ChieroNbestParser::~ChieroNbestParser()
{
	while(!nbest_groups.empty())
	{
		delete nbest_groups.back();
		nbest_groups.pop_back();
	}
}

ostream& operator<<(ostream& out, const ChieroNbestParser& parser)
{
	out<< parser.nbest_groups.size()<<endl;
	vector<CandGroups*>::const_iterator  group_iter = parser.nbest_groups.begin();
	for( ; group_iter != parser.nbest_groups.end(); group_iter++)
		out<<*(*group_iter);
	return out;
}