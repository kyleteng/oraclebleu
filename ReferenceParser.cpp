#include "ReferenceParser.h"
#include <fstream>
#include <stdlib.h>

ReferenceParser::ReferenceParser(const std::string& file)
{
	this->parse(file);
	cout<<"文件: "<<file<<"解析完毕"<<endl;
}

void ReferenceParser::parse(const std::string& file)
{
	std::ifstream fin(file.c_str());
	if(!fin)
	{
		std::cerr<<"file:"<<file<<"cannot open"<<std::endl;
		exit(-1);
	}
	std::string line;
	//<refset setid="set" srclang="Chinese" trglang="English">
	//<doc sysid="s1" docid="document">
	//<seg id="1">sure. it is flight c three zero six. </seg>
	//读第一行
	getline(fin,line);
	if(line.find("<refset") != 0)
	{
		cerr<<"format error: "<<line<<endl;
		exit(-1);
	}
	//多第一个doc
	while(getline(fin,line))
	{
		if(line.find("<doc") ==  0) 
			this->parse_doc(fin);
		else if( line.find("</refset>") == 0)
			 break;
		else
		{
			cerr<<"file format error: "<<line<<endl;
			exit(-1);
		}
	}
	fin.close();
}

void ReferenceParser::parse_doc(istream& fin)
{
	string line;
	//加1
	this->references.resize(this->references.size() + 1);

	while(getline(fin,line))
	{
		if(line.find("</doc>") == 0 )  break;
		if(line.find("<seg") == 0) 
		{
			string seg;
			this->parse_seg(line,seg);
			this->references.back().push_back(seg);
		}
		else
		{
			cerr<<"doc format error: "<<line<<endl;
		    exit(-1);
		}
	}
}

void ReferenceParser::parse_seg(const string& line, string& seg)
{
	//<seg id="921">proceed to the end of the hall way, and you will see it where the instruction sign is. </seg>
	int start = line.find(">")+1;
	if( start >= (int)line.size() )
	{
		cerr<<"seg format error: "<<line<<endl;
		exit(-1);
	}
	int end = line.find("</seg>") ;
	if( end < start)
	{
		cerr<<"seg format error: "<<line<<endl;
		exit(-1);
	}
	seg = line.substr(start,end - start);
	//cout<<seg<<endl;
}

ostream& operator<<(ostream& out, const ReferenceParser& refs)
{
	vector<DOC > ::const_iterator doc_iter = refs.references.begin();
	int  doc_id = 1;
	for(; doc_iter != refs.references.end() ;doc_iter++)
	{
		out<<"doc: "<<(doc_id++) << endl;
		vector<SEG>::const_iterator seg_iter = doc_iter->begin();
		for(; seg_iter != doc_iter->end() ; seg_iter++)
			out<< *seg_iter <<endl;
		out<<endl;
	}
	return out;
}
